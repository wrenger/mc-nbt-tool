package com.l4r0x.nbttool

import io.opencubes.boxlin.adapter.*
import net.minecraft.block.*
import net.minecraftforge.common.*
import net.minecraftforge.eventbus.api.*
import net.minecraftforge.fml.common.*
import net.minecraftforge.fml.event.lifecycle.*
import net.minecraftforge.fml.event.server.*
import net.minecraftforge.fml.javafmlmod.*
import org.apache.logging.log4j.*

// The value here should match an entry in the META-INF/mods.toml file
@Mod("nbttool")
object ModConfiguration {

    // Directly reference a log4j logger.
    val LOGGER: Logger = LogManager.getLogger()

    init {
        // Register the setup method for modloading
        BoxlinContext.get().eventBus.addListener { event: FMLCommonSetupEvent -> setup(event) }
        // Register the enqueueIMC method for modloading
        BoxlinContext.get().eventBus.addListener { event: InterModEnqueueEvent -> enqueueIMC(event) }
        // Register the processIMC method for modloading
        BoxlinContext.get().eventBus.addListener { event: InterModProcessEvent -> processIMC(event) }
        // Register the doClientStuff method for modloading
        BoxlinContext.get().eventBus.addListener { event: FMLClientSetupEvent -> doClientSetup(event) }
        // Register ourselves for server and other game events we are interested in
    }

    private fun setup(event: FMLCommonSetupEvent) {
        // some preinit code
        LOGGER.info("HELLO FROM PREINIT")
        LOGGER.info("DIRT BLOCK >> {}", Blocks.DIRT.registryName)
    }

    private fun doClientSetup(event: FMLClientSetupEvent) {
        // do something that can only be done on the client
        ModEvents.registerKeys()
    }

    private fun enqueueIMC(event: InterModEnqueueEvent) {
        // some example code to dispatch IMC to another mod
    }

    private fun processIMC(event: InterModProcessEvent) {
        // some example code to receive and process InterModComms from other mods
    }
}