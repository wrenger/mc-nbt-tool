package com.l4r0x.nbttool

import com.l4r0x.nbttool.ModConfiguration.LOGGER
import com.l4r0x.nbttool.cli.*
import com.l4r0x.nbttool.gui.*
import com.l4r0x.nbttool.nbt.*
import net.minecraft.client.*
import net.minecraft.client.settings.*
import net.minecraft.util.math.*
import net.minecraftforge.api.distmarker.*
import net.minecraftforge.client.event.*
import net.minecraftforge.client.event.InputEvent.*
import net.minecraftforge.event.*
import net.minecraftforge.eventbus.api.*
import net.minecraftforge.fml.client.registry.*
import net.minecraftforge.fml.common.*
import org.lwjgl.glfw.GLFW.*

@Mod.EventBusSubscriber(value = [Dist.CLIENT])
object ModEvents {
    private val openTool = KeyBinding("key.nbttool.nbt", GLFW_KEY_N, "key.nbttool")
    private val gameRuleTool = KeyBinding("key.nbttool.gamerule", GLFW_KEY_K, "key.nbttool")

    fun registerKeys() {
        ClientRegistry.registerKeyBinding(openTool)
        ClientRegistry.registerKeyBinding(gameRuleTool)
    }

    @SubscribeEvent
    fun onKeyInput(event: KeyInputEvent) {
        if (openTool.isPressed) {
            LOGGER.info("Open edit dialog...")
            val mc = Minecraft.getInstance()
            when (val target = mc.objectMouseOver) {
                is BlockRayTraceResult -> CLI.queryNBT(target.pos) { pos, nbt ->
                    mc.displayGuiScreen(ToolScreen(CLI.identifier(pos), nbt))
                }
                is EntityRayTraceResult -> CLI.queryNBT(target.entity.uniqueID) { entity, nbt ->
                    mc.displayGuiScreen(ToolScreen(CLI.identifier(entity), nbt))
                }
            }
        } else if (gameRuleTool.isPressed) {
            LOGGER.info("Open Game Rule Tool")
            Minecraft.getInstance().displayGuiScreen(GameRuleScreen())
        }
    }

    @SubscribeEvent(priority = EventPriority.HIGH)
    fun clientChatEvent(event: ClientChatReceivedEvent) {
        CLI.clientChatEvent(event)
    }

    @SubscribeEvent
    fun tickEvent(event: TickEvent) {
        if (event.type == TickEvent.Type.CLIENT && event.phase == TickEvent.Phase.START) {
            CLI.tick()
        }
    }
}