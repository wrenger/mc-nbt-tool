package com.l4r0x.nbttool.cli

import com.l4r0x.nbttool.ModConfiguration.LOGGER
import com.l4r0x.nbttool.nbt.*
import com.mojang.brigadier.exceptions.*
import net.minecraft.client.*
import net.minecraft.nbt.*
import net.minecraft.util.math.*
import net.minecraft.util.text.*
import net.minecraftforge.client.event.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Command Line Interface for executing Chat Commands and handling their results.
 */
object CLI {

    var ticksBetweenCommands = 1
    private var tickCounter = 0

    private class CommandHandler(
            val key: String,
            val callback: (args: Array<Any>) -> Unit)

    private val handlers = ArrayList<CommandHandler>()
    private val queue = ArrayList<String>()

    fun clientChatEvent(event: ClientChatReceivedEvent) {
        val message = event.message

        if (message is TranslationTextComponent) {
            LOGGER.info("CLI: ${message.key}: ${message.formatArgs.joinToString()}")
        }
        if (handlers.isEmpty()) return

        if (event.type == ChatType.SYSTEM && message is TranslationTextComponent) {
            if (message.key == handlers.first().key) {
                handlers.removeAt(0).callback(message.formatArgs)
                event.isCanceled = true
            }
        }
    }

    fun tick() {
        if (queue.isNotEmpty()) {
            if (tickCounter <= 0) {
                val cmd = queue.removeAt(0)
                LOGGER.info("CLI execute: `$cmd`")
                Minecraft.getInstance().player.sendChatMessage(cmd)
                tickCounter = ticksBetweenCommands
            } else tickCounter--
        }
    }

    @Suppress("NOTHING_TO_INLINE")
    inline fun identifier(pos: BlockPos): String {
        return "block ${pos.x} ${pos.y} ${pos.z}"
    }

    @Suppress("NOTHING_TO_INLINE")
    inline fun identifier(entity: UUID): String {
        return "entity $entity"
    }

    fun queryGameRule(name: String, callback: (name: String, value: String) -> Unit) {
        handlers.add(CommandHandler("commands.gamerule.query") {
            if (it.size == 2) callback(name, it[1].toString())
            else LOGGER.error("Unexpected cli response: ${it.joinToString("\n")}")
        })
        queue.add("/gamerule $name")
    }

    fun updateGameRule(name: String, value: String) {
        queue.add("/gamerule $name $value")
    }

    fun queryNBT(block: BlockPos, callback: (block: BlockPos, value: CompoundNBT) -> Unit) {
        handlers.add(CommandHandler("commands.data.block.query") {
            val arg = it.getOrNull(3)
            if (arg is ITextComponent) {
                try {
                    callback(block, JsonToNBT.getTagFromJson(arg.string))
                } catch (e: CommandSyntaxException) {
                    LOGGER.error(e)
                }
            } else LOGGER.error("Unexpected cli response: ${it.joinToString("\n")}")
        })
        queue.add("/data get ${identifier(block)}")
    }

    fun queryNBT(entity: UUID, callback: (entity: UUID, value: CompoundNBT) -> Unit) {
        handlers.add(CommandHandler("commands.data.entity.query") {
            val arg = it.getOrNull(1)
            if (arg is ITextComponent) {
                try {
                    callback(entity, JsonToNBT.getTagFromJson(arg.string))
                } catch (e: CommandSyntaxException) {
                    LOGGER.error(e)
                }
            } else LOGGER.error("Unexpected cli response: ${it.joinToString("\n")}")
        })
        queue.add("/data get ${identifier(entity)}")
    }

    fun updateNBT(identifier: String, actions: List<NBTAction>) {
        queue.addAll(actions.map { it.toCommand(identifier) })
    }
}