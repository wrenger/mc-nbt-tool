package com.l4r0x.nbttool.util

import net.minecraft.util.*

fun String.getIndentation(): String {
    val indent = indexOfFirst { !it.isWhitespace() }
    return if (indent > 0) substring(0, indent) else ""
}

fun Char.isAllowed(): Boolean {
    return this == '\n' || SharedConstants.isAllowedCharacter(this)
}

fun String.filterAllowedChars(): String {
    return filter { it.isAllowed() }
}

fun Char.getCharClass(): Int {
    return if (isLetterOrDigit()) 0 else if (isWhitespace()) 1 else 2
}
