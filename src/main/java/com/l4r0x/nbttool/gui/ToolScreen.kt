package com.l4r0x.nbttool.gui

import com.l4r0x.nbttool.ModConfiguration.LOGGER
import com.l4r0x.nbttool.cli.*
import com.l4r0x.nbttool.gui.TextAreaWidget.*
import com.l4r0x.nbttool.nbt.*
import com.mojang.brigadier.exceptions.*
import net.minecraft.client.gui.screen.*
import net.minecraft.client.gui.widget.button.*
import net.minecraft.nbt.*
import net.minecraft.util.text.*
import net.minecraftforge.api.distmarker.*

@OnlyIn(Dist.CLIENT)
class ToolScreen(
        private val identifier: String,
        private val nbt: CompoundNBT)
    : Screen(StringTextComponent(identifier)) {

    private val padding = 10

    private var nbtField: TextAreaWidget? = null
    private var errorMessage: String = ""

    override fun shouldCloseOnEsc(): Boolean = true

    override fun isPauseScreen(): Boolean = true

    public override fun init() {
        super.init()
        buttons.clear()
        addButton(Button(width / 2 - 55, height - 20 - padding, 50, 20, "Close") { onClose() })
        addButton(Button(width / 2 + 5, height - 20 - padding, 50, 20, "Save") { onSave() })

        nbtField = TextAreaWidget(font, padding, padding + 20, width - 2 * padding, height - 2 * padding - 30 - 30, "nbttool.nbt")
                .apply {
                    text = nbt.toFormattedComponent("    ", 0).string
                    textFormatter = NBTFormatter()
                }

        // Event handling
        children.add(nbtField)
        func_212928_a(nbtField)
    }

    override fun tick() {
        super.tick()
        nbtField?.tick()
    }

    override fun render(mouseX: Int, mouseY: Int, delta: Float) {
        renderBackground()
        font.drawStringWithShadow(title.formattedText, padding.toFloat(), padding.toFloat(), 0xFFFFFF)

        nbtField?.render(mouseX, mouseY, delta)
        if (errorMessage.isNotEmpty()) {
            val msg = font.trimStringToWidth(errorMessage, width - 2 * padding)
            font.drawStringWithShadow(msg, padding.toFloat(), height - padding - 30.toFloat(), TextFormatting.RED.color!!)
        }
        super.render(mouseX, mouseY, delta)
    }

    private fun onSave() {
        val newNbt: CompoundNBT
        try {
            newNbt = JsonToNBT.getTagFromJson(nbtField!!.text)
            CLI.updateNBT(identifier, changes("", nbt, newNbt))
            onClose()
        } catch (e: CommandSyntaxException) {
            nbtField?.apply {
                setCursor(TextPos(0, 0))
                setCursor(jumpFromCursor(e.cursor))
            }
            LOGGER.error(e.message)
            errorMessage = e.message ?: ""
        }
    }
}