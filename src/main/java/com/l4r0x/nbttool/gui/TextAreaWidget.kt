package com.l4r0x.nbttool.gui

import com.l4r0x.nbttool.ModConfiguration.LOGGER
import com.l4r0x.nbttool.util.*
import com.mojang.blaze3d.platform.*
import net.minecraft.client.*
import net.minecraft.client.gui.*
import net.minecraft.client.gui.screen.*
import net.minecraft.client.gui.widget.*
import net.minecraft.client.renderer.*
import net.minecraft.client.renderer.vertex.*
import net.minecraft.client.resources.*
import net.minecraft.util.*
import net.minecraft.util.math.*
import org.lwjgl.glfw.GLFW.*
import java.util.function.*
import kotlin.math.*

class TextAreaWidget(
        private val font: FontRenderer,
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        msg: String)
    : Widget(x, y, width, height, msg),
        IRenderable,
        IGuiEventListener {

    data class TextPos(var row: Int, var col: Int) {
        fun min(b: TextPos): TextPos {
            return when {
                row < b.row -> this
                row > b.row -> b
                else -> TextPos(row, col.coerceAtMost(b.col))
            }
        }

        fun max(b: TextPos): TextPos {
            return when {
                row < b.row -> b
                row > b.row -> this
                else -> TextPos(row, col.coerceAtLeast(b.col))
            }
        }
    }

    var isEnabled = true
    var guiResponder: Consumer<String> = Consumer { }
    var textFormatter = BiFunction { text: String, offset: Int -> text }
    var validator = Predicate { s: String -> true }

    private var lines = ArrayList<String>().apply { add("") }
    private var isSelecting = false
    private var cursorCounter = 0

    private val cursor = TextPos(0, 0)
    private val selection = TextPos(0, 0)

    private var colScrollOffset = 0
    private var rowScrollOffset = 0

    companion object {
        private const val lineHeight = 10
        private const val border = 4
        private const val enabledColor = 0xe0e0e0
        private const val disabledColor = 0x707070
    }

    var maxRows = 1024
        set(value) {
            field = value
            if (lines.size > value) lines = ArrayList(lines.take(value))
        }

    private val innerWidth: Int
        get() = width - border - border

    private val visibleRowCount: Int
        get() = (height - border - border) / lineHeight

    var isVisible: Boolean
        get() = visible
        set(visible) {
            this.visible = visible
        }


    var text: String
        get() = lines.joinToString("\n")
        set(newText) {
            if (validator.test(newText)) {
                lines.clear()
                var newLines = newText.split('\n')
                if (newLines.size > maxRows) newLines = newLines.subList(0, maxRows)
                lines.addAll(newLines)
                val pos = TextPos(lines.size - 1, lines[lines.size - 1].length)
                setCursor(pos)
                setSelection(pos)
                onTextChange(text)
            }
        }

    val selectedText: String
        get() {
            if (cursor != selection) {
                val begin = cursor.min(selection)
                val end = cursor.max(selection)
                return if (begin.row == end.row) {
                    lines[cursor.col].substring(begin.col, end.col)
                } else {
                    val builder = StringBuilder()
                    builder.append(lines[begin.row].substring(begin.col))
                    builder.append('\n')
                    for (line in lines.subList(begin.row + 1, end.row)) {
                        builder.append(line)
                        builder.append('\n')
                    }
                    builder.append(lines[end.row], 0, end.col)
                    builder.toString()
                }
            }
            return ""
        }


    public override fun setFocused(isFocusedIn: Boolean) {
        super.setFocused(isFocusedIn)
    }

    override fun getNarrationMessage(): String {
        return if (message.isEmpty()) "" else I18n.format("gui.narrate.editBox", message, text)
    }

    fun tick() {
        ++cursorCounter
    }

    /**
     * Adds the given text after the cursor, or replaces the currently selected text if there is a
     * selection.
     */
    fun writeText(textToWrite: String) {
        var addLines = textToWrite.filterAllowedChars().split('\n')
        val begin = cursor.min(selection)
        val end = cursor.max(selection)
        val maxNewRows = maxRows - (lines.size - (end.row - begin.row))
        if (maxNewRows < 0) addLines = addLines.subList(0, -maxNewRows)

        // Prefix
        val newLines = ArrayList(lines.subList(0, begin.row))
        // Add new line to current one
        newLines.add(lines[begin.row].substring(0, begin.col) + addLines[0])
        // Add additional lines
        if (addLines.size > 1) newLines.addAll(addLines.subList(1, addLines.size))

        val newCursor = TextPos(newLines.size - 1, newLines[newLines.size - 1].length)

        // Suffix
        newLines[newLines.size - 1] = newLines[newLines.size - 1] + lines[end.row].substring(end.col)
        if (end.row < lines.size - 1) newLines.addAll(lines.subList(end.row + 1, lines.size))

        val newText: String = newLines.joinToString("\n")
        if (validator.test(newText)) {
            lines = newLines
            setCursor(newCursor)
            setSelection(newCursor)
            onTextChange(newText)
        }
    }

    private fun onTextChange(text: String) {
        guiResponder.accept(text)
        nextNarration = Util.milliTime() + 500L
    }

    fun setCursor(pos: TextPos) {
        cursor.row = MathHelper.clamp(pos.row, 0, lines.size - 1)
        cursor.col = MathHelper.clamp(pos.col, 0, lines[cursor.row].length)
        if (!isSelecting) {
            setSelection(pos)
        }
    }

    fun setSelection(pos: TextPos) {
        val text = text
        selection.row = MathHelper.clamp(pos.row, 0, lines.size - 1)
        val lineLen = lines[selection.row].length
        selection.col = MathHelper.clamp(pos.col, 0, lineLen)

        rowScrollOffset = rowScrollOffset.coerceAtMost(lines.size - 1)
        rowScrollOffset = when {
            selection.row < rowScrollOffset -> selection.row
            selection.row >= rowScrollOffset + visibleRowCount -> selection.row - visibleRowCount + 1
            else -> rowScrollOffset
        }

        colScrollOffset = MathHelper.clamp(colScrollOffset, 0, lineLen)
        val renderedText = font.trimStringToWidth(text.substring(colScrollOffset), innerWidth)
        val lastRenderedPos = colScrollOffset + renderedText.length

        colScrollOffset += when {
            selection.col > lastRenderedPos -> selection.col - lastRenderedPos
            selection.col == colScrollOffset -> -font.trimStringToWidth(text, innerWidth, true).length
            selection.col < colScrollOffset -> -(colScrollOffset - selection.col)
            else -> 0
        }
        colScrollOffset = MathHelper.clamp(colScrollOffset, 0, lineLen)
    }

    private fun delete(forward: Boolean) {
        if (Screen.hasControlDown()) deleteWords(forward)
        else deleteCharacters(if (forward) 1 else -1)
    }

    fun deleteWords(forward: Boolean) {
        if (cursor == selection) setSelection(jumpWordFromCursor(forward))
        writeText("")
    }

    fun deleteCharacters(num: Int) {
        if (cursor == selection) setSelection(jumpFromCursor(num))
        writeText("")
    }

    fun jumpFromCursor(number: Int): TextPos {
        var num = number
        if (num < 0) {
            if (cursor.col + num >= 0) {
                return TextPos(cursor.row, cursor.col + num)
            } else {
                var row = cursor.row
                num += cursor.col + 1
                while (num <= 0) {
                    row--
                    if (row < 0) return TextPos(0, 0)
                    num += lines[row].length + 1
                }
                return TextPos(row, num - 1)
            }
        } else {
            if (cursor.col + num <= lines[cursor.row].length) {
                return TextPos(cursor.row, cursor.col + num)
            } else {
                var row = cursor.row
                num -= lines[row].length - cursor.col + 1
                while (num >= 0) {
                    row++
                    if (row >= lines.size) return TextPos(row - 1, lines[row - 1].length)
                    num -= lines[row].length + 1
                }
                return TextPos(row, num + lines[row].length - 1)
            }
        }
    }

    private fun jumpWordFromCursor(forward: Boolean): TextPos {
        var row = cursor.row
        var col = cursor.col
        val text = lines[cursor.row]
        val len = text.length
        if (forward) {
            if (col < len) {
                val type = text[col].getCharClass()
                do ++col while (col < len && type == text[col].getCharClass())
            } else {
                col = if (row >= lines.size - 1) len else {
                    row++
                    0
                }
            }
        } else {
            if (col > 0) {
                val type = text[col - 1].getCharClass()
                do --col while (col > 0 && type == text[col - 1].getCharClass())
            } else {
                col = if (row <= 0) 0 else {
                    row--
                    lines[row].length
                }
            }
        }
        return TextPos(row, col)
    }

    // Events
    override fun keyPressed(key: Int, scanCode: Int, modifiers: Int): Boolean {
        if (!visible || !isFocused) return false
        isSelecting = Screen.hasShiftDown()
        when {
            Screen.isSelectAll(key) -> {
                setCursor(TextPos(lines.size - 1, lines[lines.size - 1].length))
                setSelection(TextPos(0, 0))
            }
            Screen.isCopy(key) -> {
                Minecraft.getInstance().keyboardListener.clipboardString = selectedText
            }
            Screen.isPaste(key) -> {
                if (isEnabled) writeText(Minecraft.getInstance().keyboardListener.clipboardString)
            }
            Screen.isCut(key) -> {
                Minecraft.getInstance().keyboardListener.clipboardString = selectedText
                if (isEnabled) writeText("")
            }
            else -> when (key) {
                GLFW_KEY_ENTER -> {
                    if (cursor == selection) {
                        writeText("\n" + lines[cursor.row].getIndentation())
                    } else writeText("\n")
                }
                GLFW_KEY_BACKSPACE -> if (isEnabled) delete(false)
                GLFW_KEY_DELETE -> if (isEnabled) delete(true)
                GLFW_KEY_RIGHT -> {
                    cursorCounter = 0
                    if (Screen.hasControlDown()) {
                        setCursor(jumpWordFromCursor(true))
                    } else setCursor(jumpFromCursor(1))
                }
                GLFW_KEY_LEFT -> {
                    cursorCounter = 0
                    if (Screen.hasControlDown()) {
                        setCursor(jumpWordFromCursor(false))
                    } else setCursor(jumpFromCursor(-1))
                }
                GLFW_KEY_DOWN -> {
                    cursorCounter = 0
                    if (Screen.hasControlDown()) {
                        setCursor(TextPos(lines.size - 1, lines[lines.size - 1].length))
                    } else setCursor(TextPos(cursor.row + 1, cursor.col))
                }
                GLFW_KEY_UP -> {
                    cursorCounter = 0
                    if (Screen.hasControlDown()) {
                        setCursor(TextPos(0, 0))
                    } else setCursor(TextPos(cursor.row - 1, cursor.col))
                }
                GLFW_KEY_HOME -> setCursor(TextPos(cursor.row, 0))
                GLFW_KEY_END -> setCursor(TextPos(cursor.row, lines[cursor.row].length))
                else -> {
                    LOGGER.info("TextArea Key $key")
                    return false
                }
            }
        }
        return true
    }

    override fun charTyped(input: Char, key: Int): Boolean {
        if (visible && isFocused && isEnabled && input.isAllowed()) {
            writeText(input.toString())
            return true
        }
        return false
    }

    override fun mouseClicked(mX: Double, mY: Double, button: Int): Boolean {
        if (isVisible) {
            val clickedInside = isMouseOver(mX, mY)
            isFocused = clickedInside
            if (isFocused && clickedInside && button == 0) {
                isSelecting = Screen.hasShiftDown()
                val innerX = MathHelper.floor(mX) - x - border
                var row = (MathHelper.floor(mY) - y - border) / lineHeight + rowScrollOffset
                row = MathHelper.clamp(row, 0, lines.size - 1)
                val visibleText: String = font.trimStringToWidth(lines[row].substring(colScrollOffset), innerWidth)
                val col = font.trimStringToWidth(visibleText, innerX).length + colScrollOffset
                setCursor(TextPos(row, col))
                return true
            }
        }
        return false
    }

    override fun mouseScrolled(mX: Double, mY: Double, scroll: Double): Boolean {
        if (isVisible && isMouseOver(mX, mY)) {
            if (Screen.hasShiftDown()) {
                colScrollOffset = MathHelper.clamp(colScrollOffset - scroll.roundToInt(), 0, lines[selection.row].length - 1)
            } else {
                rowScrollOffset = MathHelper.clamp(rowScrollOffset - scroll.roundToInt(), 0, lines.size - 1)
            }
            return true
        }
        return false
    }

    override fun changeFocus(focus: Boolean): Boolean {
        return visible && isEnabled && super<Widget>.changeFocus(focus)
    }

    override fun isMouseOver(mX: Double, mY: Double): Boolean {
        return visible && mX >= x && mX < x + width && mY >= y && mY < y + height
    }

    override fun onFocusedChanged(focus: Boolean) {
        if (focus) cursorCounter = 0
    }

    override fun renderButton(mouseX: Int, mouseY: Int, delta: Float) {
        if (isVisible) {
            // Border
            AbstractGui.fill(x - 1, y - 1, x + width + 1, y + height + 1, -0x5f5f60)
            AbstractGui.fill(x, y, x + width, y + height, -0x1000000)

            val textX = x + border

            val selectionBegin = cursor.min(selection)
            val selectionEnd = cursor.max(selection)

            for (i in rowScrollOffset until (rowScrollOffset + visibleRowCount).coerceAtMost(lines.size)) {
                val textY = y + border + (i - rowScrollOffset) * lineHeight
                val visibleText = if (colScrollOffset < lines[i].length) {
                    font.trimStringToWidth(lines[i].substring(colScrollOffset), innerWidth)
                } else ""

                // Draw text
                font.drawStringWithShadow(textFormatter.apply(visibleText, colScrollOffset),
                        textX.toFloat(), textY.toFloat(),
                        if (isEnabled) enabledColor else disabledColor)

                // Draw cursor
                if (isFocused && cursor.row == i && cursorCounter / 6 % 2 == 0) {
                    val relativeCursorCol = cursor.col - colScrollOffset
                    if (0 <= relativeCursorCol && relativeCursorCol <= visibleText.length) {
                        val cursorX = textX + font.getStringWidth(visibleText.substring(0, relativeCursorCol)) - 1
                        AbstractGui.fill(cursorX, textY - 1, cursorX + 1, textY + lineHeight - 1, -0x2f2f30)
                    }
                }

                // Draw selection
                if (selectionBegin != selectionEnd && selectionBegin.row <= i && i <= selectionEnd.row) {
                    val selBeginX = if (i == selectionBegin.row && selectionBegin.col > colScrollOffset) {
                        textX + font.getStringWidth(visibleText.substring(0, selectionBegin.col - colScrollOffset))
                    } else textX

                    val selEndX = if (i == selectionEnd.row && selectionEnd.col <= colScrollOffset + visibleText.length) {
                        textX + font.getStringWidth(visibleText.substring(0, selectionEnd.col - colScrollOffset))
                    } else textX + innerWidth

                    drawSelectionBox(selBeginX, textY - 1, selEndX - 1, textY + lineHeight - 1)
                }
            }
        }
    }

    private fun drawSelectionBox(startX: Int, startY: Int, endX: Int, endY: Int) {
        val tessellator: Tessellator = Tessellator.getInstance()
        val buffer: BufferBuilder = tessellator.buffer
        GlStateManager.color4f(0.0f, 0.0f, 255.0f, 255.0f)
        GlStateManager.disableTexture()
        GlStateManager.enableColorLogicOp()
        GlStateManager.logicOp(GlStateManager.LogicOp.OR_REVERSE)
        buffer.begin(7, DefaultVertexFormats.POSITION)
        buffer.pos(startX.toDouble(), endY.toDouble(), 0.0).endVertex()
        buffer.pos(endX.toDouble(), endY.toDouble(), 0.0).endVertex()
        buffer.pos(endX.toDouble(), startY.toDouble(), 0.0).endVertex()
        buffer.pos(startX.toDouble(), startY.toDouble(), 0.0).endVertex()
        tessellator.draw()
        GlStateManager.disableColorLogicOp()
        GlStateManager.enableTexture()
    }
}