package com.l4r0x.nbttool.gui

import com.l4r0x.nbttool.cli.*
import com.mojang.blaze3d.platform.*
import net.minecraft.client.*
import net.minecraft.client.gui.*
import net.minecraft.client.gui.widget.*
import net.minecraft.client.gui.widget.list.*
import net.minecraft.util.*
import net.minecraft.world.*
import org.lwjgl.glfw.GLFW.*

/**
 * Editable List of GameRules
 *
 * @constructor Create a new GameRule List Widget
 *
 * @param minecraft Minecraft instance
 * @param width Widget width
 * @param height Widget height
 * @param y0 Y Pos where the list ScrollBox begins
 * @param y1 Y Pos where the list ScrollBox ends
 */
class GameRuleList(
        minecraft: Minecraft,
        width: Int,
        height: Int,
        y0: Int,
        y1: Int)
    : AbstractOptionList<GameRuleList.Entry>(minecraft, width, height, y0, y1, rowHeight) {

    companion object {
        const val rowHeight = 24
        const val widgetWidth = 100
        const val padding = 60
    }

    init {
        class RuleEntryVisitor(private val callback: (key: String, value: GameRules.RuleValue<*>) -> Unit)
            : GameRules.IRuleEntryVisitor {

            override fun <T : GameRules.RuleValue<T>> func_223481_a(key: GameRules.RuleKey<T>, type: GameRules.RuleType<T>) {
                // func_223576_a: Rule Name
                // func_223579_a: Rule Value
                callback(key.func_223576_a(), type.func_223579_a())
            }
        }
        // Iterate over all GameRules Minecraft defines and query their current value
        GameRules.func_223590_a(RuleEntryVisitor { key, _ ->
            CLI.queryGameRule(key, this::addGameRule)
        })
    }

    // Add the GameRule with the given value
    private fun addGameRule(key: String, value: String) {
        val num = value.toIntOrNull()
        if (num != null) {
            addEntry(IntEntry(key, num))
        } else {
            addEntry(BoolEntry(key, value.toBoolean()))
        }
    }

    // Width of the the rows inside the scroll box
    override fun getRowWidth(): Int = width - padding - padding

    // Horizontal position of the scrollbar
    override fun getScrollbarPosition(): Int = width - padding - 5

    fun tick() {
        for (child in children()) child.tick()
    }

    fun onClose() {
        // Deselect all elements to trigger save events on focus loss
        for (child in children()) child.changeFocus(false)
    }

    override fun mouseClicked(mX: Double, mY: Double, button: Int): Boolean {
        // Deselect all elements to trigger save events on focus loss
        // Also preventing the issue of selecting multiple text fields
        for (child in children()) child.changeFocus(false)
        return super.mouseClicked(mX, mY, button)
    }

    // GameRule entry
    abstract class Entry(
            internal val key: String)
        : AbstractOptionList.Entry<Entry>() {

        protected val font: FontRenderer = Minecraft.getInstance().fontRenderer

        override fun render(idx: Int, rowTop: Int, rowLeft: Int, rowWidth: Int, rowHeight: Int,
                            mouseX: Int, mouseY: Int, mouseOver: Boolean, delta: Float) {
            font.drawString(key, rowLeft.toFloat(), rowTop.toFloat() + 7, 0xFFFFFF)
        }

        open fun tick() {}
    }

    // GameRule entry for bool values
    class BoolEntry(
            key: String,
            private var value: Boolean)
        : Entry(key) {

        companion object {
            // Texture of the recipe books 'craftable' toggle
            val TOGGLE_TEXTURE = ResourceLocation("textures/gui/recipe_book.png")
        }

        private val toggle = ToggleWidget(0, 0, 26, 16, value).apply {
            message = "nbttool.gr.$key"
            initTextureValues(152, 41, 28, 18, TOGGLE_TEXTURE);
        }

        private fun onToggle() {
            value = !value
            toggle.isStateTriggered = value
            CLI.updateGameRule(key, value.toString())
        }

        override fun render(idx: Int, rowTop: Int, rowLeft: Int, rowWidth: Int, rowHeight: Int,
                            mouseX: Int, mouseY: Int, mouseOver: Boolean, delta: Float) {

            toggle.x = rowLeft + rowWidth - widgetWidth - 10
            toggle.y = rowTop + 2
            // Workaround: TextFieldWidget sets color which may affect the texture rendering of this toggle
            GlStateManager.color4f(255.0F, 255.0F, 255.0F, 255.0F)
            toggle.render(mouseX, mouseY, delta)

            super.render(idx, rowTop, rowLeft, rowWidth, rowHeight, mouseX, mouseY, mouseOver, delta)
        }

        override fun children(): MutableList<out IGuiEventListener> {
            return listOf(toggle).toMutableList()
        }

        // Hand input events over to the toggle widget

        override fun changeFocus(focus: Boolean): Boolean {
            if (toggle.isFocused == focus) return false
            return super.changeFocus(focus)
        }

        override fun keyPressed(key: Int, scanCode: Int, modifiers: Int): Boolean {
            when (key) {
                GLFW_KEY_E,
                GLFW_KEY_SPACE,
                GLFW_KEY_ENTER -> if (toggle.isFocused) {
                    onToggle()
                    return true
                }
            }
            return false
        }

        override fun mouseClicked(mx: Double, my: Double, btn: Int): Boolean {
            if (toggle.mouseClicked(mx, my, btn)) {
                onToggle()
                return true
            }
            return false
        }

        override fun mouseReleased(mx: Double, my: Double, btn: Int): Boolean {
            return toggle.mouseReleased(mx, my, btn)
        }
    }

    // GameRule entry for int values
    class IntEntry(
            key: String,
            private var value: Int)
        : Entry(key) {

        private val field = TextFieldWidget(font, 0, 0, widgetWidth, rowHeight - 4, "nbttool.gr.$key")
                .apply { text = "$value" }

        override fun tick() {
            if (field.isFocused) field.tick()
        }

        override fun render(idx: Int, rowTop: Int, rowLeft: Int, rowWidth: Int, rowHeight: Int,
                            mouseX: Int, mouseY: Int, mouseOver: Boolean, delta: Float) {

            field.x = rowLeft + rowWidth - widgetWidth - 10
            field.y = rowTop + 2
            field.render(mouseX, mouseY, delta)

            super.render(idx, rowTop, rowLeft, rowWidth, rowHeight, mouseX, mouseY, mouseOver, delta)
        }

        // Hand input events over to the text field widget

        override fun keyPressed(key: Int, p_keyPressed_2_: Int, p_keyPressed_3_: Int): Boolean {
            return field.keyPressed(key, p_keyPressed_2_, p_keyPressed_3_)
        }

        override fun charTyped(char: Char, p_charTyped_2_: Int): Boolean {
            return field.charTyped(char, p_charTyped_2_)
        }

        override fun changeFocus(focus: Boolean): Boolean {
            if (field.isFocused == focus) return false
            if (!focus) {
                val newValue = field.text.toIntOrNull()
                if (newValue != null && value != newValue) {
                    value = newValue
                    CLI.updateGameRule(key, value.toString())
                }
            }
            return field.changeFocus(focus)
        }

        override fun children(): MutableList<out IGuiEventListener> {
            return listOf(field).toMutableList()
        }

        override fun mouseClicked(mx: Double, my: Double, btn: Int): Boolean {
            return field.mouseClicked(mx, my, btn)
        }
    }
}