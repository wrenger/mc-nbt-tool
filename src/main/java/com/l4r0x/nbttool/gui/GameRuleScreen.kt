package com.l4r0x.nbttool.gui

import net.minecraft.client.gui.screen.*
import net.minecraft.client.gui.widget.button.*
import net.minecraft.util.text.*

class GameRuleScreen
    : Screen(StringTextComponent("World Game Rules")) {

    private val padding = 10

    private var ruleList: GameRuleList? = null

    override fun shouldCloseOnEsc(): Boolean = true

    override fun isPauseScreen(): Boolean = true

    // Create and add widgets
    override fun init() {
        super.init()

        buttons.clear()
        addButton(Button(width / 2 - 50, height - padding - 20, 100, 20, "Close") { onClose() })

        ruleList = GameRuleList(minecraft!!, width, height, padding + 15, height - padding - 25)
        children.add(ruleList)
    }

    // On mc tick (for time based widget updates)
    override fun tick() {
        super.tick()
        ruleList?.tick();
    }

    // Render widgets and draw additional items
    override fun render(mouseX: Int, mouseY: Int, delta: Float) {
        renderDirtBackground(0)

        ruleList?.render(mouseX, mouseY, delta)

        drawCenteredString(font, title.formattedText, width / 2, padding, 0xFFFFFF)

        super.render(mouseX, mouseY, delta)
    }

    // When the screen is close (cleanup and save changes)
    override fun onClose() {
        ruleList?.onClose()
        super.onClose()
    }
}