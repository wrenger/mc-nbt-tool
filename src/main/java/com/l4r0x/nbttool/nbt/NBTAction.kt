package com.l4r0x.nbttool.nbt

import com.l4r0x.nbttool.nbt.*
import net.minecraft.nbt.*

abstract class NBTAction protected constructor(
        protected val path: String) {

    abstract fun toCommand(target: String): String

    class ListAdd(
            path: String,
            private val idx: Int,
            private val value: INBT)
        : NBTAction(path) {

        override fun toCommand(target: String): String {
            return "/data modify $target $path insert $idx value $value"
        }
    }

    class ListSet(
            path: String,
            idx: Int,
            private val value: INBT)
        : NBTAction(pathAdd(path, idx)) {

        override fun toCommand(target: String): String {
            return "/data modify $target $path set value $value"
        }
    }

    class ListRemove(
            path: String,
            idx: Int)
        : NBTAction(pathAdd(path, idx)) {

        override fun toCommand(target: String): String {
            return "/data remove $target $path"
        }
    }

    class DictSet(
            path: String,
            key: String,
            val value: INBT)
        : NBTAction(pathAdd(path, key)) {

        override fun toCommand(target: String): String {
            return "/data modify $target $path set value $value"
        }
    }

    class DictRemove(
            path: String,
            key: String)
        : NBTAction(pathAdd(path, key)) {

        override fun toCommand(target: String): String {
            return "/data remove $target $path"
        }
    }

    class DictMerge(
            path: String,
            private val value: INBT)
        : NBTAction(path) {

        override fun toCommand(target: String): String {
            return "/data modify $target $path merge value $value"
        }
    }

    companion object {

        fun pathAdd(path: String, idx: Int): String {
            return "$path[$idx]"
        }

        fun pathAdd(path: String, key: String): String {
            return if (path.isEmpty()) key else "$path.$key"
        }

        private fun mergeActions(actions: List<DictSet>): DictMerge {
            val nbt = CompoundNBT()
            val path = if ('.' in actions[0].path) actions[0].path.substringBeforeLast('.') else "/"

            for (action in actions) {
                val key = action.path.let { if ('.' in it) it.substringAfterLast('.') else it }
                nbt[key] = action.value
            }

            return DictMerge(path, nbt)
        }

        fun reduceActions(actions: List<NBTAction>): List<NBTAction> {
            return actions.groupBy { it.path }
                    .flatMap {
                        return if (it.value.all { it is DictSet }) {
                            listOf(mergeActions(it.value as List<DictSet>))
                        } else it.value
                    }
        }
    }
}