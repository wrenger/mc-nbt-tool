package com.l4r0x.nbttool.nbt

import com.l4r0x.nbttool.nbt.NBTAction.*
import com.l4r0x.nbttool.nbt.NBTAction.Companion.pathAdd
import net.minecraft.nbt.*
import java.util.*


operator fun CompoundNBT.set(key: String, value: INBT) {
    put(key, value)
}

fun changes(path: String, oldNbt: ListNBT, newNbt: ListNBT): List<NBTAction> {
    val actions = ArrayList<NBTAction>()

    // TODO: Detect insertions and Removals

    var i = 0
    while (i < oldNbt.size && i < newNbt.size) {
        val oldVal = oldNbt[i]
        val newVal = newNbt[i]
        if (oldVal is ListNBT && newVal is ListNBT) {
            actions.addAll(changes(pathAdd(path, i), oldVal, newVal))
        } else if (oldVal is CompoundNBT && newVal is CompoundNBT) {
            actions.addAll(changes(pathAdd(path, i), oldVal, newVal))
        } else if (oldVal != newVal) {
            actions.add(ListSet(path, i, newVal))
        }
        ++i
    }
    while (i < oldNbt.size) {
        actions.add(ListRemove(path, i))
        ++i
    }
    while (i < newNbt.size) {
        actions.add(ListAdd(path, i, newNbt[i]))
        ++i
    }
    return actions
}

fun changes(path: String, oldNbt: CompoundNBT, newNbt: CompoundNBT): List<NBTAction> {
    val actions = ArrayList<NBTAction>()
    val keys = HashSet(oldNbt.keySet()) + newNbt.keySet()
    for (key in keys) {
        val oldVal = if (key in oldNbt) oldNbt[key] else null
        val newVal = if (key in newNbt) newNbt[key] else null
        if (oldVal != null && newVal != null) {
            if (oldVal is ListNBT && newVal is ListNBT) {
                actions.addAll(changes(pathAdd(path, key), oldVal, newVal))
            } else if (oldVal is CompoundNBT && newVal is CompoundNBT) {
                actions.addAll(changes(pathAdd(path, key), oldVal, newVal))
            } else if (oldVal != newVal) {
                actions.add(DictSet(path, key, newVal))
            }
        } else if (newVal != null) {
            actions.add(DictSet(path, key, newVal))
        } else if (oldVal != null) {
            actions.add(DictRemove(path, key))
        }
    }
    return actions
}